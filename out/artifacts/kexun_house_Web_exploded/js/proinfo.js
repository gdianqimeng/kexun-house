$(function() {
	// 获取参数id
	var id = getParam("id");

	$.get("/MyHouseRental/getHouseInfo", {
		"id" : id
	}, function(result) {
		$(".proTitle").html(result.houseName);
		$("#bh").html(result.houseID);
		$("#sj").html(result.housePrice);
		$("#hx").html(result.housetype.type);
		$("#mj").html(result.houseArea);
		$("#cx").html(result.houseOrientation);
		$("#lc").html(result.houseFloor);
		$("#zx").html(result.fITMENTType);
		$("#xq").html(result.houseXQ);
		$(".proList").html(result.houseDetails);
		$("#img").attr('src', '../images/' + result.houseImage + '');
	});

	// 参数获取方法
	function getParam(ParmaName) {
		var url = document.location.toString();

		var arrObj = url.split("?");
		if (arrObj.length >= 1) {

			var arrParams = arrObj[1].split("&");

			for (var i = 0; i < arrParams.length; i++) {

				var arr = arrParams[i].split("=");

				if (arr[0] == ParmaName) {
					return arr[1];
				}

			}

			return "";
		}
	}

	// 获取房源区域
	$.get("/MyHouseRental/locationInfo", function(result) {

		$.each(result, function(index, item) {
			$("#location").append(
					"<a href='javascript:;'>" + item.locationName + "</a>");

		});

	});

	$("#attentionHouse").click(function() {
		// 关注房源按钮
		$.post("/MyHouseRental/userAddAttentionHouse", {
			"houseID" : id
		}, function(result) {
			if (result.message == "noLogin") {
				
				$.message({
					type : 'warning',
					message : '你还没有登录哦'
				});
			}else{
				if(result.message!="haveAttention"){
				if(result.message=="ok"){
					$.message({
						type : 'success',
						message : '关注成功,您可以到[我的关注]查看'
					});
				}else{
					$.message({
						type : 'error',
						message : '关注失败,原因可能是服务端出现错误,请联系站长'
					});
				}
				}else{
					$.message({
						type : 'success',
						message : '您已经关注了哦!'
					});
				}
				
			}

		});

	});

});