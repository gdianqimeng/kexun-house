$(function() {
	// 获取房源区域
	$.ajaxSetup({
		async : false
	});

	$.get("/MyHouseRental/locationInfo", function(result) {
		$("#location2").append(
				" <a href='javascript:;' class='pro-cur' value='0' >不限</a>");
		$.each(result, function(index, item) {

			$("#location1").append(
					"<a href='javascript:;'  value=" + item.locationID + ">"
							+ item.locationName + "</a>");
			$("#location2").append(
					"<a href='javascript:;'  value=" + item.locationID + ">"
							+ item.locationName + "</a>");

		});

	});

	// 定义几个高级查询的变量
	var locationID;
	var price;
	// var rentWay;
	var area;
	var type;
	var orderBy;
	//加载时查询所有
	gjcx();
	// 给所有的地区绑定单击事件
	var locations = $("#location2 a");
	$.each(locations, function(index, item) {
		$(item).click(function() {
			var value = $(item).attr('value');
			locationID = value;
			gjcx();
		});

	});

	// 给所有的租金选项绑定单机事件
	var priceBtns = $("#priceBtn a");
	$.each(priceBtns, function(index, item) {
		$(item).click(function() {
			var value = $(item).attr('value');
			price = value;
			gjcx();
		});

	});

	$(".proSub").click(function() {

		var value1 = $("#t1").val();

		var value2 = $("#t2").val();
		value = "";

		if (value1 == "") {
			value += 0;

		} else {

			value += value1;
		}

		if (value2 == "") {

			value += "-" + 999999999;
		} else {

			value += "-" + value2
		}

		price = value;

		gjcx();

	});
	// 给所有的出租方式绑定单机事件
	/*
	 * var rentWays = $("#rentWay a"); $.each(rentWays, function(index, item) {
	 * $(item).click(function() { var value = $(item).attr('value'); rentWay =
	 * value; gjcx(); });
	 * 
	 * });
	 */

	// 给所有的面积绑定单机事件
	var areas = $("#area a");
	$.each(areas, function(index, item) {
		$(item).click(function() {
			var value = $(item).attr('value');
			area = value;
			gjcx();
		});

	});

	// 给所有的房型绑定单机事件
	var types = $("#type a");
	$.each(types, function(index, item) {
		$(item).click(function() {
			var value = $(item).attr('value');
			type = value;
			gjcx();
		});

	});

	// 给所有的排序绑定单机事件
	var orderBys = $("#orderBy a");
	$.each(orderBys, function(index, item) {
		$(item).click(function() {
			var value = $(item).attr('value');
			orderBy = value;
			gjcx();
		});

	});

	// 高级查询通用方法
	// 定义几个高级查询的变量
	/*
	 * var locationID; var price; var rentWay; var area; var type; var orderBy;
	 */
	function gjcx() {
		$
				.ajax({
					url : "/MyHouseRental/superHouseList",
					data : {
						"locationID" : locationID,
						"price" : price,
						"area" : area,
						"type" : 3,
						"orderBy" : orderBy,
						"houseType" : type
					},
					type : "GET",
					success : function(result) {

						$("#houseList").html("");
						$.each(result,function(index, item) {
											$("#houseList").append(
															"<dl>"
																	+ "<dt>"
																	+ "	<a href='houseInfo?id="+item.houseID+"'><img src='../images/"
																	+ item.houseImage
																	+ "' width='286'"
																	+ "	height='188' /></a>"
																	+ "	</dt>"
																	+ "<dd>"
																	+ "	<h3>"
																	+ "	<a href='houseInfo?id="
																	+ item.houseID
																	+ "'>"
																	+ item.houseName
																	+ "</a>"
																	+ "</h3>"
																	+ "	<div class='pro-wei'>"
																	+ "<img src='images/weizhi.png' width='12' height='16' /> <strong"
																	+ "	class='red'>"
																	+ item.location.locationName
																	+ "</strong>"
																	+ "</div>"
																	+ "	<div class='pro-fang'>"
																	+ item.housetype.type
																	+ " "
																	+ item.houseArea
																	+ "平  "
																	+ item.houseOrientation
																	+ " 楼层"
																	+ item.housePart
																	+ " "
																	+ item.houseFloor
																	+ "层</div>"
																	+ "	<div class='pra-fa'>发布人："
																	+ item.linkMan
																	+ " 发布时间："
																	+ item.releaseTime
																	+ "</div>"
																	+ "</dd>"
																	+ "<div class='price'>"
																	+ "¥ <strong>"
																	+ item.housePrice
																	+ "</strong><span class='font12'>元/月</span>"
																	+ "</div>"
																	+ "<div class='clears'></div>"
																	+ "</dl>");

										});

					}

				});

	}
	
	// 查询新房源
	$.get("/MyHouseRental/houseList", {
		"type" : "0"
	}, function(result) {
		$("#newHouseList").html("");
		$.each(result, function(index, item) {

			$("#newHouseList").append("<dl>"
					+"<dt>"
					+"<a href='houseInfo?id="+item.houseID+"'><img src='../images/"+item.houseImage+"' /></a>"
					+"</dt>"
					+"<dd>"
					+"<h3>"
					+"<a href='houseInfo?id="+item.houseID+"'>"+item.houseName+"</a>"
					+"</h3>"
					+"<div class='pro-fang'>"+item.housetype.type+" "+item.houseArea+"平 "+item.houseOrientation+"</div>"
					+"<div class='right-price'>"+item.housePrice+"元/月</div>"
					+"</dd>"
					+"</dl>");

		});

	});
	
	//指定购房
	$("#sub").click(function(){
		var buyType=$("#buyType").val();
		var phone=$("#phone").val();
		var locationID=$("#locationID").val();
		
		$.post("/MyHouseRental/specifybuy",{"buyType":buyType,"phone":phone,"locationID":locationID},function(result){
			
			if(result.message="ok"){
				$.message({
			        message:'预约成功,请保持电话畅通,客服将在24小时内联系您',
			        type:'success'
			    });
				
				//关闭
				$(".bg100").hide();
				$(".zhidinggoufang").fadeOut();
				
			}else{
				
				$.message({
			        message:'系统错误,添加失败',
			        type:'error'
			    });
			}
			
			
		});
		
	});

})