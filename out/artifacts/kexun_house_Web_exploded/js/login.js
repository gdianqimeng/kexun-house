$(function(){
	//登录
	$("#login").click(function(){
	
		var userName=$("[name='username']").val();
		var userPassword=$("[name='password']").val();
		if(userName==""){
			
			 $.message({
			        message:'账号不能为空',
			        type:'error'
			    });
			 return;
		}
		if(userPassword==""){
			
			 $.message({
			        message:'密码不能为空',
			        type:'error'
			    });
			 return;
		}
		
		
		$.post("/MyHouseRental/login",{"userName":userName,"userPassword":userPassword},function(result){
			if(result.success=="ok"){
				
				window.location.href="/MyHouseRental/index";
			}else{
				  $.message({
				        message:'密码错误',
				        type:'error'
				    });
			}
			
			
			
		});
			
	
	});
	
	//获取房源区域
	$.get("/MyHouseRental/locationInfo",function(result){
		
		$.each(result,function(index,item){
			$("#location").append("<a href='javascript:;'>"+item.locationName+"</a>");
			
			
		});
		
	});
	
	
});