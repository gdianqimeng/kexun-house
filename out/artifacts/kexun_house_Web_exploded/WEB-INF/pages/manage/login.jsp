<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>后台登录</title>
    <meta name="author" content="DeathGhost"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/message.css"/>
    <style>
        body {
            height: 100%;
            background: #16a085;
            overflow: hidden;
        }

        canvas {
            z-index: -1;
            position: absolute;
        }
    </style>
    <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/js/message.js"></script>

    <script src="${pageContext.request.contextPath}/js/verificationNumbers.js"></script>
    <script src="${pageContext.request.contextPath}/js/Particleground.js"></script>
    <script>
        $(document).ready(function () {
            //粒子背景特效
            $('body').particleground({
                dotColor: '#5cbdaa',
                lineColor: '#5cbdaa'
            });

            //测试提交，对接程序删除即可

        });
    </script>
</head>
<body>
<dl class="admin_login">
    <dt>
        <strong>站点后台管理系统</strong>
        <em>Management System</em>
    </dt>
    <dd class="user_icon">
        <input type="text" placeholder="账号" class="login_txtbx" name="manageName" id="manageName"/>
    </dd>
    <dd class="pwd_icon">
        <input type="password" placeholder="密码" class="login_txtbx" name="managePassword" id="managePassword"/>
    </dd>

    <dd>
        <input type="button" value="立即登录" class="submit_btn" id="login"/>
    </dd>
    <dd>
        <p>© 2015-2020 DeathGhost 版权所有</p>
        <p>湘A-2017-8</p>
    </dd>
</dl>
</body>
<script type="text/javascript">

    $(function () {
        var path = '${pageContext.request.contextPath}';
        $("#login").click(function () {

            var manageName = $("#manageName").val();
            var managePassword = $("#managePassword").val();

            if (manageName == "") {
                $.message({
                    type: 'error',
                    message: '请输入用户名'

                });
                return;
            }
            if (managePassword == "") {

                $.message({
                    type: 'error',
                    message: '请输入密码'

                });

                return;
            }
            $.post(path + "/manage/login", {
                "manageName": manageName,
                "managePassword": managePassword
            }, function (result) {
                if (result.message == "success") {
                    window.location.href = path + '/manage';
                } else {

                    $.message({
                        type: 'error',
                        message: '用户名或密码错误'

                    });

                }


            });

        });


    });


</script>

</html>
