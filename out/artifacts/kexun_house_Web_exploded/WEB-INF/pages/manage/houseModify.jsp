<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<title>后台管理系统</title>
<meta name="author" content="DeathGhost" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/message.css">
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/js/message.js"></script>
<script src="${pageContext.request.contextPath}/js/houseModify.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" charset="utf-8"
	src="${pageContext.request.contextPath}/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="${pageContext.request.contextPath}/ueditor/ueditor.all.min.js">
	
</script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8"
	src="${pageContext.request.contextPath}/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>

<body>
	<!--header-->
	<header>
		<h1>
			<img src="${pageContext.request.contextPath}/images/admin_logo.png" />
		</h1>
		<ul class="rt_nav">
			<li><a href="#" target="_blank" class="website_icon">站点首页</a></li>
			<li><a href="#" class="set_icon">账号设置</a></li>
			<li><a href="${pageContext.request.contextPath}/manage/loginOut" class="quit_icon">安全退出</a></li>
		</ul>
	</header>
	<!--aside nav-->
	<!--aside nav-->
	<aside class="lt_aside_nav content mCustomScrollbar">
		<h2>
			<a href="${pageContext.request.contextPath}/manage">起始页</a>
		</h2>
		<ul>
			<li>
				<dl>
					<dt>房屋管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/zu">出租房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/er">二手房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/xin">全新房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/houseAdd">发布信息</a>
					</dd>

				</dl>

				<dl>
					<dt>区域管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/location_list">区域列表</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/locationAdd">添加区域</a>
					</dd>

				</dl>

				<dl>
					<dt>预约管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/yuyue">预约列表</a>
					</dd>


				</dl>

				<dl>
					<dt>用户管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/user_list">用户列表</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/user_detail">添加管理员</a>
					</dd>


				</dl>
			</li>


		</ul>
	</aside>

	<section class="rt_wrap content mCustomScrollbar">
		<div class="rt_content">
			<div class="page_title">
				<h2 class="fl">发布房屋信息</h2>
				<a class="fr top_rt_btn" href="product_list.html">返回房屋列表</a>
			</div>
			<section>
				<form id="houseInfo">
					<input type="hidden" value="${house.houseID}" name="houseID">
					<ul class="ulColumn2">
						<li><span class="item_name" style="width: 120px;">房屋标题：</span>
							<input type="text" class="textbox textbox_295"
							placeholder="房屋标题..." name="houseName" value="${house.houseName}" />
							<span class="errorTips"></span></li>
						<li><span class="item_name" style="width: 120px;">房屋面积：</span>
							<input type="text" class="textbox" placeholder="房屋面积..."
							name="houseArea" value="${house.houseArea}" /> (平方) <span
							class="errorTips"></span></li>
						<li><span class="item_name" style="width: 120px;">所在区域：</span>
							<select class="select" style="width: 80px" name="locationID">
								<c:forEach items="${locationList }" var="location">
									<option
										${house.locationID==location.locationID?"selected='selected'":""}
										value=${location.locationID }>${location.locationName }</option>
								</c:forEach>


						</select></li>
						<li><span class="item_name" style="width: 120px;">房型：</span>
							<select class="select" style="width: 80px" name="houseTypeID">
								<option ${house.houseTypeID==1?"selected='selected'":"" }
									value="1">一室一厅</option>
								<option ${house.houseTypeID==2?"selected='selected'":"" }
									value="2">两室一厅</option>
								<option ${house.houseTypeID==3?"selected='selected'":"" }
									value="3">三室一厅</option>
								<option ${house.houseTypeID==4?"selected='selected'":"" }
									value="4">一室一厅一卫</option>
								<option ${house.houseTypeID==5?"selected='selected'":"" }
									value="5">其他</option>
						</select></li>
						<li><span class="item_name" style="width: 120px;">房屋朝向：</span>
							<select class="select" style="width: 80px"
							name="houseOrientation">
								<option
									${house.houseOrientation eq "东"?"selected='selected'":"" }>东</option>
								<option
									${house.houseOrientation eq "东"?"selected='selected'":"" }>南</option>
								<option
									${house.houseOrientation eq "东"?"selected='selected'":"" }>西</option>
								<option
									${house.houseOrientation eq "东"?"selected='selected'":"" }>北</option>
						</select></li>

						<li><span class="item_name" style="width: 120px;">装修类型：</span>
							<select class="select" style="width: 80px" name="fITMENTType">
								<option ${house.fITMENTType eq "精装"?"selected='selected'":"" }
									value="精装">精装</option>
								<option ${house.fITMENTType eq "普通"?"selected='selected'":"" }
									value="普通">普通</option>
								<option ${house.fITMENTType eq "毛呸房"?"selected='selected'":"" }
									value="毛呸房">毛呸房</option>
								<option ${house.fITMENTType eq "其他"?"selected='selected'":"" }
									value="其他">其他</option>
						</select></li>

						<li><span class="item_name" style="width: 120px;">出售类型：</span>
							<label class="single_selection"><input type="radio"
								name="type" value="1" ${house.type==1?"checked='checked'":""} />出租</label>
							<label class="single_selection"><input type="radio"
								name="type" ${house.type==3?"checked='checked'":""} value="3" />二手房</label>
							<label class="single_selection"><input type="radio"
								name="type" value="2" ${house.type==2?"checked='checked'":""} />新房</label></li>

						<%-- <li><span class="item_name" style="width: 120px;">出租方式：</span>
							<label class="single_selection"><input type="radio"
								name="hentWay" ${house.hentWay==1?"checked='checked'":""}
								value="1" />短租</label> <label class="single_selection"><input
								type="radio" name="hentWay" value="2"
								${house.hentWay==2?"checked='checked'":""} />长租</label> <label
							class="single_selection"><input type="radio"
								name="hentWay" value="3"
								${house.hentWay==3?"checked='checked'":""} />合租</label> <label
							class="single_selection"> <input type="radio"
								name="hentWay" value="4"
								${house.hentWay==4?"checked='checked'":""} />个人
						</label> <label class="single_selection"> <input type="radio"
								name="hentWay" value="5" />其他
						</label></li> --%>

						<li><span class="item_name" style="width: 120px;">联系人：</span>
							<input type="text" class="textbox textbox_295"
							placeholder="联系人..." name="linkMan" value="${house.linkMan }" />
							<span class="errorTips"></span></li>
						<li><span class="item_name" style="width: 120px;">售价：</span>
							<input type="text" class="textbox " placeholder="售价..."
							name="housePrice" value="${ house.housePrice}" /> <span
							class="errorTips"></span></li>
						<li><span class="item_name" style="width: 120px;">所属小区：</span>
							<input type="text" class="textbox " placeholder="小区名称..."
							name="houseXQ" value="${house.houseXQ }" /> <span
							class="errorTips"></span></li>
						<li><span class="item_name" style="width: 120px;">所属楼层：</span>
							<input type="text" class="textbox " placeholder="所属楼层..."
							name="houseFloor" value="${house.houseFloor }" /> (楼) <span
							class="errorTips"></span></li>
						<li><span class="item_name" style="width: 120px;">楼层部位：</span>
							<select class="select" style="width: 80px" name="housePart">
								<option ${house.housePart eq "上"?"selected='selected'":"" }>上</option>
								<option ${house.housePart eq "中"?"selected='selected'":"" }>中</option>
								<option ${house.housePart eq "下"?"selected='selected'":"" }>下</option>
						</select></li>
						<li><span class="item_name" style="width: 120px;">标题图片：</span>
							<label class="uploadImg"> <input type="file" name="hImg" />
								<span>上 &nbsp;&nbsp;&nbsp;传</span>
						</label></li>



						<li><span class="item_name" style="width: 120px;">详情：</span>
							<input type="button" value="编辑" id="edi"
							style="background-color: #1E90FF" class="link_btn"><span
							class="errorTips"></span></li>



						<li><script id="editor" type="text/plain"
								style="width: 1024px; height: 500px;">

						</script></li>


						<li><span class="item_name" style="width: 120px;"></span> <input
							type="button" value="提交" class="link_btn" id="sub" /></li>

					</ul>
				</form>
			</section>
		</div>
	</section>
</body>
<script type="text/javascript">
	$(function() {

		$("#edi").click(function() {
			

			var ue = UE.getEditor('editor');
			ue.ready(function() {
				ue.setContent('${house.houseDetails }');
			});

		});

	});
</script>
</html>