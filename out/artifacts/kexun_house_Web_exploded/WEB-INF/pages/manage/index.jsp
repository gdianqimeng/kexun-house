<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>后台管理系统</title>
<meta name="author" content="DeathGhost" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css">
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.mCustomScrollbar.concat.min.js"></script>
<style>
.title {
	width: 800px;
	height: 400px;
	background-color: #19a97b;
	font-size: 70px;
	text-align: center;
	line-height: 400px;
	position: relative;
	top: 15%;
	left: 29%;
	color: white;
}
</style>
</head>
<body>
	<!--header-->
	<header>
		<h1>
			<img src="${pageContext.request.contextPath}/images/admin_logo.png" />
		</h1>
		<ul class="rt_nav">
			<li><a href="#" target="_blank" class="website_icon">站点首页</a></li>
			<li><a href="#" class="set_icon">账号设置</a></li>
			<li><a href="${pageContext.request.contextPath}/manage/loginOut" class="quit_icon">安全退出</a></li>
		</ul>
	</header>
	<!--aside nav-->
	<!--aside nav-->
	<aside class="lt_aside_nav content mCustomScrollbar">
		<h2>
			<a href="${pageContext.request.contextPath}/manage">起始页</a>
		</h2>
		<ul>
			<li>
				 <dl>
					<dt>房屋管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/zu">出租房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/er">二手房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/xin">全新房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/houseAdd">发布信息</a>
					</dd>

				</dl>

				<dl>
					<dt>区域管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/location_list">区域列表</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/locationAdd">添加区域</a>
					</dd>

				</dl>

				<dl>
					<dt>预约管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/yuyue">预约列表</a>
					</dd>


				</dl>

				<dl>
					<dt>用户管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/user_list">用户列表</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/user_detail">添加管理员</a>
					</dd>


				</dl>
		</ul>
	</aside>

	<style>
.dataStatistic {
	width: 400px;
	height: 200px;
	border: 1px solid #ccc;
	margin: 0 auto;
	margin: 10px;
	overflow: hidden
}

#cylindrical {
	width: 400px;
	height: 200px;
	margin-top: -15px
}

#line {
	width: 400px;
	height: 200px;
	margin-top: -15px
}

#pie {
	width: 400px;
	height: 200px;
	margin-top: -15px
}
</style>
	<div class="title">邻居大妈后台管理系统</div>

</body>
</html>
