# 科讯租房网

#### 介绍
本项目包含用户端和管理端,采用JSP模板引擎渲染页面,使用Ajax实现前后端异步交互,用户端包含房屋列表,房屋详情,房屋筛选,租房,二手房,新房可按租金,出租方式,面积,房型进行筛选 按价格,发布时间进行排序,并提供登录,注册,收藏房源等功能;管理端提供登录,房源管理,区域管理,预约管理,管理员,用户管理,发布房源信息等功能

#### 软件架构
环境 Java8  Maven3.6  IDEA

Spring SpringMVC Mybatis  百度富文本编辑器  html  css  JavaScript


sql下载: [立即下载](http://muzidong.com/productDetail/8ff44c71db6b4b6aa30c71e646b1c558)

##技术支持:
[技术支持](http://muzidong.com/productDetail/80044c71db6b4b6aa30c71e646b1c559)



[更多项目下载](http://muzidong.com/product)

