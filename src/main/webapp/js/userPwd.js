$(function() {
	// 修改密码

	$("#subBtn").click(function() {
		var oldPassword = $("#old_password").val();
		var newPassword = $("#new_password").val();
		var newPassword1 = $("#new_password1").val();
		if (oldPassword == "") {
			$.message({
				message : '旧密码不能为空 ',
				type : 'error',
			});
			return;

		}

		if (newPassword == "") {
			$.message({
				message : '新密码不能为空 ',
				type : 'error',
			});
			return;

		}

		if (newPassword1 == "") {
			$.message({
				message : '确认密码不能为空 ',
				type : 'error',
			});
			
			
			return;

		}
		
		if(newPassword!=newPassword1){
			$.message({
				message : '两次输入密码不一致 ',
				type : 'error',
			});
			
			return;
		}

		$.post("/MyHouseRental/userpwdModify", {
			"newUserPassword" : newPassword,
			"oldUserPassword" : oldPassword
		}, function(result) {
			if (result.message == "ok") {
				$.message({
					message : '密码已更新',
					type : 'success',
					showClose : true

				});

				setTimeout(function() {

					window.location.href = "index";

				}, 3000);

			} else {
				if (result.message == "passwordError") {
					$.message({
						message : '旧密码错误 ',
						type : 'error',
						showClose : true

					});

				} else {
					$.message({
						message : '密码修改失败: ',
						type : 'error',
						showClose : true

					});
				}

			}

		});

	});

});