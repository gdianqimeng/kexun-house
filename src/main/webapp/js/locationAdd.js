$(function(){
	
	$(".errorTips").removeClass("errorTips");
	

	$("#sub").click(function(){
		var locationName=$("#locationName").val();
		if(locationName==""){
			
			$(".errorTips").addClass("errorTips");
			$(".errorTips").text("区域名称不能为空");
			return;
		}
		$.post("/MyHouseRental/manage/locationAdd",{"locationName":locationName},function(result){
			
			if(result.message=="success"){
				
				$.message({
					type:'success',
					message:'区域保存成功'
					
				});
				
				setTimeout(function() {

					window.location.href = "/MyHouseRental/manage/location_list";

				}, 2000);
				
			}else{
				$.message({
					type:'error',
					message:'系统错误'
					
				});
				
				
			}
			
		});
		
		
		
	});
	
	
	
});