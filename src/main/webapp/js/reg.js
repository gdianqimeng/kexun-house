$(function(){
	
	$("#reg").click(function(){
	
		var username=$("#agent").val();
		var password=$("#password").val();
		var password1=$("#confirm_password").val();
		if(username==""){
			 $.message({
			        message:'用户名不能为空',
			        type:'error'
			    });
			 return;
			
		}
		
		if(password==""){
			 $.message({
			        message:'密码不能为空',
			        type:'error'
			    });
			 return;
				
			
		}
		
		if(password1==""){
			 $.message({
			        message:'请输入确认密码',
			        type:'error'
			    });
			 return;
				
			
		}
		
		if(password!=password1){
			 $.message({
			        message:'两次输入密码不一致',
			        type:'error'
			    });
			 return;
				
			
		}
		
		$.post("/MyHouseRental/reg",{"username":username,"password":password},function(result){
			
			if(result.success!="have"){
				if(result.success=="ok"){
					window.location.href="/MyHouseRental/index";
					
				}else{
					
					 $.message({
					        message:'系统错误',
					        type:'error'
					    });
					 return;
					
				}
				
				
			}else{
				
				
					 $.message({
					        message:'此用户名已经存在',
					        type:'error'
					    });
					
						
				
				
			}
			
			
		});
		
		
		
	});

});