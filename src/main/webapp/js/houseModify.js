$(function() {
	
	
	$(".errorTips").removeClass("errorTips");
	var inputs=$(":input");
	$.each(inputs,function(index,item){
		
		$(item).blur(function(){
			if($(item).val()==""){
				$(item).next().addClass("errorTips");
				$(item).next().text("此项不能为空");
			}
			
		});
		
		$(item).focus(function(){
			$(item).next().text("");
			$(item).next().removeClass("errorTips");
			
		});
		
	});
	
	var tag=true;
	$("#sub").click(function() {

		
		
		$.each(inputs,function(index,item){
			if($(item).val()==""&&$(item).attr('type')!="file"){
				$(item).next().addClass("errorTips");
				$(item).next().text("此项不能为空");
				tag=false;
				return false
				
			}
			
			
			
		});
		if(!tag){
			return;
		}
		
		
		
		$.ajax({
			url : "/MyHouseRental/manage/houseModify",
			type : "post",
			data : new FormData($('#houseInfo')[0]),
			processData : false,
			contentType : false,
			/*
			 * beforeSend : function() { $(".member_mod_buttom").val("信息提交中");
			 * $(".member_mod_buttom").attr('disabled', true);
			 *  },
			 */
			success : function(result) {
					if(result.message=="success"){
						$.message({
							type:'success',
							message:'信息保存成功'
							
						});
						setTimeout(function() {

							window.location.href = "/MyHouseRental/manage/zu";

						}, 2000);
						
						
						
					}else{
						$.message({
							type:'error',
							message:'系统错误'
							
						});
					}
			}

		});

	});

});