package com.kexun.filter;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class ManageFilter implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object handler)
            throws Exception {
        if (req.getSession().getAttribute("manage") == null) {
            System.out.println("拦截请求");
            resp.setContentType("text/html;caharset=utf-8");
            PrintWriter out = resp.getWriter();
            String contextPath = req.getContextPath();
            out.write("<script>window.location.href='" + contextPath + "/manage/login'</script>");
            out.close();
            return false;

        } else {

            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterCompletion(HttpServletRequest req, HttpServletResponse resp, Object handler, Exception ex)
            throws Exception {


    }

}
