package com.kexun.filter;


import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class UserInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object arg2) throws Exception {
		//拦截请求
		if(req.getSession().getAttribute("user")==null) {
			System.out.println("拦截请求");
			resp.setContentType("text/html;caharset=utf-8");
			PrintWriter out = resp.getWriter();
			out.write("<script>alert('请先登录');window.location.href='" + req.getContextPath() + "'/manage/login'</script>");
			out.close();
			return false;
			
		}else {
			
			return true;	
		}
		
		
	}


}
