package com.kexun.control;

import com.kexun.entity.House;
import com.kexun.entity.Location;
import com.kexun.mapper.houseMapper.houseMapper;
import com.kexun.mapper.locationMapper.LocationMapper;
import com.kexun.util.PageBreak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class HouseControl {
    @Autowired
    private houseMapper houseDao;
    @Autowired
    private LocationMapper locationDao;

    @ResponseBody
    @RequestMapping("/houseList")
    public List<House> queryTop4HouseByType(@RequestParam(name = "type", required = true) int type) {
        List<House> queryTop4HouseByType = houseDao.queryTop4HouseByType(type);
        return queryTop4HouseByType;
    }

    @RequestMapping("/houseInfo")
    public String goHouseInfo() {
        return "front/proinfo";
    }

    @ResponseBody
    @RequestMapping("/getHouseInfo")
    public House getHouseInfo(@RequestParam(name = "id", required = true) int id) {
        House house = houseDao.queryHouseByID(id);
        return house;
    }

    @RequestMapping("/zu")
    public String gozu() {
        return "front/pro_zu";
    }

    @RequestMapping("/xin")
    public String goxin() {
        return "front/pro_xin";
    }

    @RequestMapping("/er")
    public String goer() {
        return "front/pro_er";
    }

    @RequestMapping("/about")
    public String goabout() {
        return "front/about";
    }

    @ResponseBody
    @RequestMapping("/superHouseList")
    public List<House> queryZuList(String locationID, String price, String rentWay, String area, String type,
                                   String orderBy, String houseType) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 房屋出售类型
        if (type != null) {
            map.put("type", Integer.parseInt(type));
        } else {
            map.put("type", 0);
        }

        // 出租区域
        if (locationID != null) {
            map.put("locationID", Integer.parseInt(locationID));
        } else {
            map.put("locationID", 0);
        }

        // 出租价格
        if (price != null && !"0".equals(price)) {
            String prices[] = price.split("-");
            System.err.println(prices[0]);
            map.put("housePriceg", Integer.parseInt(prices[0]));
            map.put("housePricel", Integer.parseInt(prices[1]));
        } else {
            map.put("housePriceg", 0);
            map.put("housePricel", 0);
        }

        // 出租方式
        if (rentWay != null && !"0".equals(rentWay)) {
            map.put("rentWay", Integer.parseInt(rentWay));
        } else {
            map.put("rentWay", 0);
        }

        // 出租面积
        if (area != null && !"0".equals(area)) {
            String[] areas = area.split("-");
            if (areas.length > 0) {
                map.put("houseArea1", Integer.parseInt(areas[0]));
            } else {
                map.put("houseArea1", 0);
            }
            if (areas.length > 1) {
                map.put("houseArea2", Integer.parseInt(areas[1]));
            } else {
                map.put("houseArea2", 0);
            }

        } else {
            map.put("houseArea1", 0);
            map.put("houseArea2", 0);
        }

        // 户型
        if (houseType != null && !"0".equals(houseType)) {
            map.put("houseType", Integer.parseInt(houseType));
        } else {
            map.put("houseType", 0);
        }

        // 排序
        if (orderBy != null && !"0".equals(orderBy)) {
            if ("1".equals(orderBy)) {
                map.put("orderBy", "house.`HousePrice`");
            }
        } else {
            map.put("orderBy", null);
        }

        List<House> houseList = houseDao.queryZuList(map);

        return houseList;
    }


    @RequestMapping("/manage/zu")
    public ModelAndView toZu(@RequestParam(name = "page", defaultValue = "1") int page, @RequestParam(name = "hName", required = false) String hName) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", 1);
        map.put("hName", hName);
        List<House> queryZuList = houseDao.queryZuList(map);
        queryZuList = PageBreak.fy(page, queryZuList, 10);
        ModelAndView mv = new ModelAndView("manage/zu");
        mv.addObject("zuList", queryZuList);
        mv.addObject("pageCount", PageBreak.getPageCount());
        mv.addObject("page", page);
        return mv;
    }

    @RequestMapping("/manage/er")
    public ModelAndView toER(@RequestParam(name = "page", defaultValue = "1") int page, @RequestParam(name = "hName", required = false) String hName) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", 3);
        map.put("hName", hName);
        List<House> queryErList = houseDao.queryZuList(map);
        queryErList = PageBreak.fy(page, queryErList, 10);
        ModelAndView mv = new ModelAndView("manage/er");
        mv.addObject("erList", queryErList);
        mv.addObject("page", page);
        mv.addObject("pageCount", PageBreak.getPageCount());
        return mv;
    }

    @RequestMapping("/manage/xin")
    public ModelAndView toXin(@RequestParam(name = "page", defaultValue = "1") int page, @RequestParam(name = "hName", required = false) String hName) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", 2);
        map.put("hName", hName);
        List<House> queryXinList = houseDao.queryZuList(map);
        queryXinList = PageBreak.fy(page, queryXinList, 10);
        ModelAndView mv = new ModelAndView("manage/xin");
        mv.addObject("xinList", queryXinList);
        mv.addObject("pageCount", PageBreak.getPageCount());
        mv.addObject("page", page);
        return mv;
    }

    @RequestMapping(value = "/manage", method = RequestMethod.GET)
    public ModelAndView toManage() {
        ModelAndView mv = new ModelAndView("manage/index");
        return mv;
    }


    //只跳转到页面
    @RequestMapping(value = "/manage/houseAdd", method = RequestMethod.GET)
    public ModelAndView tohouseAdd() {
        //查询区域
        List<Location> queryAllLocations = locationDao.queryAllLocations();
        ModelAndView mv = new ModelAndView("manage/houseAdd");
        mv.addObject("locationList", queryAllLocations);
        return mv;
    }


    //执行添加
    @ResponseBody
    @RequestMapping(value = "/manage/houseAdd", method = RequestMethod.POST)
    public Map<String, String> houseAdd(House house, @RequestParam("hImg") MultipartFile houseImage, HttpServletRequest req) {
        HashMap<String, String> messageMap = new HashMap<String, String>();
        UUID uuid = UUID.randomUUID();
        long mostSignificantBits = uuid.getMostSignificantBits();
        String filename = mostSignificantBits + houseImage.getOriginalFilename();
        String path = "D:/apache-tomcat-8.5.47/webapps/images/" + filename;
        System.out.println(path);
        try {
            houseImage.transferTo(new File(path));
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        house.setHouseImage(filename);
        house.setReleaseTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        boolean addHouse = houseDao.addHouse(house);
        if (addHouse) {
            messageMap.put("message", "success");
        } else {

            messageMap.put("message", "error");

        }
        System.err.println(house);
        return messageMap;
    }


    @ResponseBody
    @ExceptionHandler
    public Map<String, String> exception(Exception ex) {
        ex.printStackTrace();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("error", ex.getLocalizedMessage());
        return map;
    }

    @ResponseBody
    @RequestMapping("/manage/delhouse")
    public Map<String, String> delHouse(@RequestParam(name = "id") int houseID) {
        Map<String, String> messageMap = new HashMap<String, String>();
        boolean delHouse = houseDao.delHouse(houseID);
        if (delHouse) {

            messageMap.put("message", "success");
        } else {

            messageMap.put("message", "error");

        }
        return messageMap;
    }

    @RequestMapping(value = "/manage/houseModify", method = RequestMethod.GET)
    public ModelAndView goHouseModify(@RequestParam("id") int id) {

        ModelAndView modelAndView = new ModelAndView("/manage/houseModify");
        List<Location> queryAllLocations = locationDao.queryAllLocations();
        modelAndView.addObject("locationList", queryAllLocations);
        House queryHouseByID = houseDao.queryHouseByID(id);
        queryHouseByID.setHouseDetails(queryHouseByID.getHouseDetails().replaceAll("\n", ""));
        modelAndView.addObject("house", queryHouseByID);
        return modelAndView;

    }


    @ResponseBody
    @RequestMapping(value = "/manage/houseModify", method = RequestMethod.POST)
    public Map<String, String> updHouse(House house, @RequestParam("hImg") MultipartFile houseImage, HttpServletRequest req) {
        HashMap<String, String> messageMap = new HashMap<String, String>();
        if (!houseImage.isEmpty()) {

            UUID uuid = UUID.randomUUID();
            long mostSignificantBits = uuid.getMostSignificantBits();
            String filename = mostSignificantBits + houseImage.getOriginalFilename();
            String path = "D:/apache-tomcat-8.5.47/webapps/images/" + filename;
            try {
                houseImage.transferTo(new File(path));
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            house.setHouseImage(filename);
            house.setReleaseTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        } else {

            house.setHouseImage(null);
            house.setReleaseTime(null);


        }
        boolean updHouse = houseDao.updHouse(house);
        if (updHouse) {
            messageMap.put("message", "success");
        } else {

            messageMap.put("message", "error");

        }
        System.err.println(house);
        return messageMap;


    }


}
