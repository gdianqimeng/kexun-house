package com.kexun.control;

import com.kexun.entity.Attention;
import com.kexun.entity.Manage;
import com.kexun.entity.Specifybuy;
import com.kexun.entity.User;
import com.kexun.mapper.userMapper.UserMapper;
import com.kexun.util.PageBreak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
public class UserControl {
    @Autowired
    private UserMapper userDao;

    // 跳转登录界面
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {


        return "front/login";

    }

    // 执行真正的登录
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Map<String, String> login(String userName, String userPassword, HttpServletRequest req) {
        Map<String, String> user = new HashMap<String, String>();
        user.put("userName", userName);
        user.put("userPassword", userPassword);
        User login = userDao.login(user);

        Map<String, String> retrunMap = new HashMap<String, String>();
        if (login != null) {
            // 设置session
            HttpSession session = req.getSession();
            session.setMaxInactiveInterval(30000);
            session.setAttribute("user", login);
            retrunMap.put("success", "ok");
        } else {
            retrunMap.put("success", "no");
        }
        return retrunMap;

    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String goIndex() {
        // 到index界面
        return "front/index";

    }

    @RequestMapping(value = "/attention", method = RequestMethod.GET)
    public String goguanzhu() {
        // 到关注界面
        return "front/user_guanzhu";

    }

    @RequestMapping(value = "/jingji", method = RequestMethod.GET)
    public String gojinji() {

        return "front/user_jingji";

    }

    @RequestMapping(value = "/shenqing", method = RequestMethod.GET)
    public String goshenqing() {
        // 到关注界面
        return "front/user_shenqing";

    }

    // 到用户界面
    @RequestMapping(value = "/user")
    public String gouser() {

        return "front/user";

    }

    // 到关注密码修改
    @RequestMapping(value = "/userpwd", method = RequestMethod.GET)
    public String gouser_pwd() {

        return "front/user_pwd";

    }

    // 到关注界面
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String gocontact() {

        return "front/contact";

    }

    // 获取用户信息
    @ResponseBody
    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public Map<String, Object> getUserInfo(String userName, String userPassword, HttpServletRequest req) {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        Map<String, Object> retrunMap = new HashMap<String, Object>();
        if (user != null) {
            retrunMap.put("success", "ok");
            retrunMap.put("user", user);
        } else {
            retrunMap.put("success", "no");
        }
        return retrunMap;

    }

    //到注册界面
    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public String reg() {

        return "front/reg";

    }

    // 用户注册
    @ResponseBody
    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public Map<String, String> reg(@RequestParam(name = "username", required = true) String userName,
                                   @RequestParam(name = "password", required = true) String userPassword, HttpServletRequest req) {

        Map<String, String> retrunMap = new HashMap<String, String>();

        Map<String, String> user = new HashMap<String, String>();
        user.put("userName", userName);
        user.put("userPassword", userPassword);

        User suffReg = userDao.suffReg(userName);

        if (suffReg == null) {
            boolean reg = userDao.reg(user);
            if (reg) {
                User user2 = new User();
                user2.setUserName(userName);
                // 设置session
                HttpSession session = req.getSession();
                session.setMaxInactiveInterval(30000);
                session.setAttribute("user", user2);
                retrunMap.put("success", "ok");
            } else {
                retrunMap.put("success", "no");
            }
        } else {
            retrunMap.put("success", "have");

        }
        return retrunMap;

    }

    // 修改用户
    @ResponseBody
    @RequestMapping(value = "/userModify", method = RequestMethod.POST)
    public Map<String, String> userModify(User user, HttpServletRequest req, @RequestParam("file") MultipartFile file) {
        Map<String, String> map = new HashMap<String, String>();

        // 阐述随机数
        UUID uuid = UUID.randomUUID();
        long mostSignificantBits = uuid.getMostSignificantBits();
        if (!file.isEmpty()) {
            String fileName = mostSignificantBits + file.getOriginalFilename();
            user.setUserImage(fileName);
            // 保存图片到名称数据库
            // 修改用户
            // 把头像保存到图片服务器
            String path = req.getRealPath("../images/" + fileName);
            System.err.println(path);
            try {

                boolean userModify = userDao.userModify(user);
                if (userModify) {
                    // 保存到本地
                    file.transferTo(new File(path));
                    map.put("success", "ok");
                } else {
                    map.put("success", "no");

                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                map.put("success", e.getLocalizedMessage());
            }

        } else {

            user.setUserImage(null);
            boolean userModify = userDao.userModify(user);
            if (userModify) {
                // 保存到本地
                map.put("success", "ok");
            } else {
                map.put("success", "no");

            }

        }
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(30000);
        session.setAttribute("user", user);
        return map;

    }

    // 关注房源
    @ResponseBody
    @RequestMapping(value = "/userAddAttentionHouse", method = RequestMethod.POST)
    public Map<String, String> AddUserAttentionHouse(
            @RequestParam(name = "houseID", required = true, defaultValue = "0") int houseID, HttpServletRequest req) {

        Map<String, String> messageMap = new HashMap<String, String>();
        if (houseID != 0) {

            HttpSession session = req.getSession();
            User user = (User) session.getAttribute("user");
            if (user != null) {
                // 查询是否已经存在了
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String aDate = format.format(new Date());

                Map<String, Object> map = new HashMap<String, Object>();
                map.put("houseID", houseID);
                map.put("userID", user.getUserID());
                map.put("aDate", aDate);
                Attention attention = userDao.queryAHByuIDAndHID(map);
                if (attention == null) {
                    boolean addUserAttentionHouse = userDao.addUserAttentionHouse(map);
                    if (addUserAttentionHouse) {

                        messageMap.put("message", "ok");

                    } else {
                        messageMap.put("message", "no");

                    }
                } else {
                    messageMap.put("message", "haveAttention");
                }
            } else {
                messageMap.put("message", "noLogin");

            }
        } else {
            messageMap.put("message", "缺少参数");
        }
        return messageMap;

    }

    // 添加用户指定购房
    @ResponseBody
    @RequestMapping(value = "/specifybuy", method = RequestMethod.POST)
    public Map<String, String> specifybuy(Specifybuy specifybuy, HttpServletRequest req) {

        Map<String, String> messageMap = new HashMap<String, String>();

        // 不登录
        boolean addUserspecifybuy = userDao.addUserspecifybuy(specifybuy);

        if (addUserspecifybuy) {
            messageMap.put("message", "ok");

        } else {
            messageMap.put("message", "no");
        }

        return messageMap;

    }

    ;

    // 用户修改密码
    @ResponseBody
    @RequestMapping(value = "/userpwdModify", method = RequestMethod.POST)
    public Map<String, String> userpwdModify(String newUserPassword, String oldUserPassword, HttpServletRequest req) {
        Map<String, String> messageMap = new HashMap<String, String>();

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");

        if (user != null) {
            if (user.getUserPassword().equals(oldUserPassword)) {

                Map<String, Object> map = new HashMap<String, Object>();
                map.put("userID", user.getUserID());
                map.put("userPassword", newUserPassword);
                boolean userpwdModify = userDao.userpwdModify(map);
                if (userpwdModify) {

                    messageMap.put("message", "ok");

                } else {
                    messageMap.put("message", "no");

                }
            } else {

                messageMap.put("message", "passwordError");

            }

        } else {

            messageMap.put("message", "noLogin");
        }

        return messageMap;
    }


    // 查询用户关注的房源
    @ResponseBody
    @RequestMapping(value = "/userAttention")
    public Map<String, Object> queryUserAtByType(String type, HttpServletRequest req) {
        Map<String, Object> meaasgeMap = new HashMap<String, Object>();
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("type", type);
            map.put("userID", user.getUserID());
            List<Attention> attentionList = userDao.queryUserAtByType(map);

            meaasgeMap.put("message", attentionList);

        } else {

            meaasgeMap.put("message", "noLogin");
        }
        return meaasgeMap;

    }

    ;


    @RequestMapping(value = "manage/yuyue")
    public ModelAndView toSpecifybuy(@RequestParam(name = "page", defaultValue = "1") int page) {

        ModelAndView modelAndView = new ModelAndView("manage/yuyue");
        List<Specifybuy> queryAllSpecifybuy = userDao.queryAllSpecifybuy();
        queryAllSpecifybuy = PageBreak.fy(page, queryAllSpecifybuy, 5);
        modelAndView.addObject("specifybuyList", queryAllSpecifybuy);
        modelAndView.addObject("pageCount", PageBreak.getPageCount());
        modelAndView.addObject("page", page);
        return modelAndView;
    }

    @RequestMapping(value = "manage/user_list")
    public ModelAndView toUserList(@RequestParam(name = "page", defaultValue = "1") int page) {

        ModelAndView modelAndView = new ModelAndView("manage/user_list");
        List<User> queryAllUser = userDao.queryAllUser();
        queryAllUser = PageBreak.fy(page, queryAllUser, 5);
        modelAndView.addObject("userList", queryAllUser);
        modelAndView.addObject("pageCount", PageBreak.getPageCount());
        modelAndView.addObject("page", page);
        return modelAndView;
    }

    //user_detail
    @RequestMapping(value = "manage/user_detail", method = RequestMethod.GET)
    public ModelAndView toUserAdd() {
        ModelAndView modelAndView = new ModelAndView("manage/user_detail");
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping("/manage/delSpecifybuy")
    public Map<String, String> delSpecifybuy(Integer id) {

        Map<String, String> messageMap = new HashMap<String, String>();
        boolean delSpecifybuy = userDao.delSpecifybuy(id);
        if (delSpecifybuy) {
            messageMap.put("message", "success");

        } else {
            messageMap.put("message", "error");

        }

        return messageMap;

    }

    @ResponseBody
    @RequestMapping("/manage/delUser")
    public Map<String, String> delUser(Integer id) {

        Map<String, String> messageMap = new HashMap<String, String>();
        boolean deluser = userDao.delUser(id);
        if (deluser) {
            messageMap.put("message", "success");

        } else {
            messageMap.put("message", "error");

        }

        return messageMap;

    }

    @ResponseBody
    @RequestMapping("manage/addManage")
    public Map<String, String> addManage(Manage manage) {
        Map<String, String> messageMap = new HashMap<String, String>();

        boolean addManage = userDao.addManage(manage);
        if (addManage) {
            messageMap.put("message", "success");

        } else {

            messageMap.put("message", "error");

        }

        return messageMap;
    }

    @ResponseBody
    @RequestMapping(value = "manage/login", method = RequestMethod.POST)
    public Map<String, String> manageLogin(Manage manage, HttpServletRequest req) {
        Map<String, String> messageMap = new HashMap<String, String>();

        Manage login = userDao.queryManageByNameandPwd(manage);
        if (login != null) {
            messageMap.put("message", "success");
            HttpSession session = req.getSession();
            session.setMaxInactiveInterval(30000);
            session.setAttribute("manage", login);

        } else {

            messageMap.put("message", "error");

        }

        return messageMap;
    }


    @RequestMapping(value = "manage/login", method = RequestMethod.GET)
    public String toManageLogin() {

        return "manage/login";
    }

    @RequestMapping(value = "manage/loginOut", method = RequestMethod.GET)
    public String toManageLoginOut(HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.invalidate();
        return "manage/login";
    }


}
