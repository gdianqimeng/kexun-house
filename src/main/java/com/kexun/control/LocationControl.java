package com.kexun.control;

import com.kexun.entity.Location;
import com.kexun.mapper.locationMapper.LocationMapper;
import com.kexun.util.PageBreak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class LocationControl {

	@Autowired
	private LocationMapper locationDao;

	// 查询所有Location
	@ResponseBody
	@RequestMapping("/locationInfo")
	public List<Location> queryAllLocation() {

		List<Location> queryAllLocations = locationDao.queryAllLocations();

		return queryAllLocations;

	}

	@RequestMapping("manage/location_list")
	public ModelAndView toLocation(@RequestParam(name="page",defaultValue="1") int page) {

		List<Location> queryAllLocations = locationDao.queryAllLocations();
		ModelAndView modelAndView = new ModelAndView("/manage/location_list");
		queryAllLocations= PageBreak.fy(page, queryAllLocations, 5);
		modelAndView.addObject("pageCount", PageBreak.getPageCount());
		modelAndView.addObject("page", page);
		modelAndView.addObject("locationList", queryAllLocations);
		return modelAndView;

	}

	// locationAdd
	@RequestMapping(value = "manage/locationAdd", method = RequestMethod.GET)
	public ModelAndView tolocationAdd() {

		ModelAndView modelAndView = new ModelAndView("/manage/locationAdd");
		return modelAndView;

	}

	@ResponseBody
	@RequestMapping(value = "manage/locationAdd", method = RequestMethod.POST)
	public Map<String, String> locationAdd(@RequestParam(name = "locationName") String locationName) {
		Map<String, String> messageMap = new HashMap<String, String>();
		boolean addLocation = locationDao.AddLocation(locationName);
		if (addLocation) {
			messageMap.put("message", "success");
		} else {
			messageMap.put("message", "error");
		}
		return messageMap;

	}
	@ResponseBody
	@RequestMapping("manage/delLocation")
	public Map<String, String> delLocation(@RequestParam(name = "id", defaultValue = "-1") int id) {

		Map<String, String> messageMap = new HashMap<String, String>();
		boolean delLocation = locationDao.delLocation(id);

		if (delLocation) {

			messageMap.put("message", "success");
		} else {

			messageMap.put("message", "error");

		}
		return messageMap;

	}
	
	//到修改页面
		@RequestMapping("/manage/locationModify")
		public ModelAndView toLocationModify(int id) {
			
			ModelAndView modelAndView = new ModelAndView("/manage/locationModify");
			
			Location queryLocatinByID = locationDao.queryLocatinByID(id);
			modelAndView.addObject("location", queryLocatinByID);
			return modelAndView;
			
		}
		
		//执行在修改
		@ResponseBody
		@RequestMapping(value="/manage/locationModify",method=RequestMethod.POST)
		public Map<String,String> locationModify(Location location){
			Map<String,String> messageMap=new HashMap<String,String>();
			boolean locationModify = locationDao.locationModify(location);
			if(locationModify) {
				
				messageMap.put("message","success");
			}else {
				messageMap.put("message","error");
			}
			
			
			return messageMap;
		}

}
