package com.kexun.util;

import java.util.ArrayList;
import java.util.List;

public class PageBreak {
	private static int pagecount;
	public static <T> List<T> fy(int page, List<T> list,int PAGESIZE){
		if(page<=0) {
			
			return null;
		}
		List<T> oList=new ArrayList<T>();
		
		pagecount=list.size()%PAGESIZE==0?list.size()/PAGESIZE:list.size()/PAGESIZE+1;
		
		int start=PAGESIZE*(page-1);
		int end=0;
		if(list.size()-start>=PAGESIZE){
		end=start+PAGESIZE;
			
		}else{
			end=list.size();
		}
		
		for (int i = start; i < end; i++) {
			oList.add(list.get(i));
		}
		return oList;
	}
	public static int getPageCount() {
		return pagecount;
	}
}
