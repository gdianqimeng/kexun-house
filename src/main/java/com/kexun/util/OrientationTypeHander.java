package com.kexun.util;

import com.kexun.enums.OrientationEnum;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class OrientationTypeHander extends BaseTypeHandler<OrientationEnum> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, OrientationEnum parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setInt(i, parameter.getOrientation());
	}

	@Override
	public OrientationEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return OrientationEnum.getNamebyOrientation(rs.getInt(columnName));
	}

	@Override
	public OrientationEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return OrientationEnum.getNamebyOrientation(rs.getInt(columnIndex));
	}

	@Override
	public OrientationEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return OrientationEnum.getNamebyOrientation(cs.getInt(columnIndex));
	}

}
