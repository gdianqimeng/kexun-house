package com.kexun.util;

import com.kexun.enums.PartEnum;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class IntToStringHander extends BaseTypeHandler<PartEnum> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, PartEnum parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setInt(i,parameter.getPart());
		
	}

	@Override
	public PartEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		// TODO Auto-generated method stub
		
		
		return PartEnum.getPartEnumByPart(rs.getInt(columnName));
	}

	@Override
	public PartEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return PartEnum.getPartEnumByPart(rs.getInt(columnIndex));
	}

	@Override
	public PartEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return PartEnum.getPartEnumByPart(cs.getInt(columnIndex));
	}

	


}
