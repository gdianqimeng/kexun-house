package com.kexun.enums;

public enum OrientationEnum {

	东(1, "东"), 南(2, "南"), 西(3, "西"), 北(4, "北");

	private int orientation;
	private String name;

	private OrientationEnum(int orientation, String name) {
		this.orientation = orientation;
		this.name = name;
	}

	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static OrientationEnum getNamebyOrientation(int orientation) {

		for (OrientationEnum o : OrientationEnum.values()) {

			if (o.getOrientation() == orientation) {
				return o;
			}

		}
		return null;

	}

}
