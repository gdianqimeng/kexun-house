package com.kexun.enums;

public enum PartEnum {
	
	上(1,"上"),中(2,"中"),下(3,"下");
	
	private int part;
	private String name;
	
	private PartEnum(int part, String name) {
		this.part = part;
		this.name = name;
	}

	public int getPart() {
		return part;
	}

	public void setPart(int part) {
		this.part = part;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static PartEnum getPartEnumByPart(int part){
		
		for(PartEnum partEnum : PartEnum.values() ) {
			if(partEnum.getPart()==part) {
				
				return partEnum;
			}
			
		}
		return null;
		
	}
	
	
}
