package com.kexun.mapper.locationMapper;


import com.kexun.entity.Location;

import java.util.List;

public interface LocationMapper {
	List<Location> queryAllLocations();
	boolean AddLocation(String locationName);
	boolean delLocation(int id);
	boolean locationModify(Location location);
	Location queryLocatinByID(int id);
}
