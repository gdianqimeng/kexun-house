package com.kexun.mapper.houseMapper;

import com.kexun.entity.House;

import java.util.List;
import java.util.Map;


public interface houseMapper {
	List<House> queryTop4HouseByType(int type);
	House queryHouseByID(int id);
	List<House> queryZuList(Map<String, Object> houseMap);
	boolean addHouse(House house);
	boolean delHouse(int houseID);
	boolean updHouse(House house);
}
