package com.kexun.mapper.userMapper;


import com.kexun.entity.Attention;
import com.kexun.entity.Manage;
import com.kexun.entity.Specifybuy;
import com.kexun.entity.User;

import java.util.List;
import java.util.Map;

public interface UserMapper {
	
	//登录
	User login(Map<String, String> user);

	//判断是否存在
	User suffReg(String username);

	//注册
	boolean reg(Map<String, String> user);

	//修改用户
	boolean userModify(User user);

	//添加关注房源
	boolean addUserAttentionHouse(Map<String, Object> map);

	//查询是否已经关注
	Attention queryAHByuIDAndHID(Map<String, Object> map);

	//用户指定购房 add
	boolean addUserspecifybuy(Specifybuy specifybuy);

	//userpwdModify
	boolean userpwdModify(Map<String, Object> map);

	//查询用户关注 参数: 用户id 出售类型id
	List<Attention> queryUserAtByType(Map<String, Object> map);
	
	//查询预约
	List<Specifybuy> queryAllSpecifybuy();
	
	List<User> queryAllUser();
	
	boolean delSpecifybuy(int id);
	
	boolean delUser(int userID);
	
	boolean addManage(Manage manage);
	
	//管理员登录
	Manage queryManageByNameandPwd(Manage manage);
	
}
