package com.kexun.entity;


import com.kexun.enums.OrientationEnum;
import com.kexun.enums.PartEnum;

public class House {
	private int	houseID;
	private String	houseName;
	private int	 houseTypeID;
	private int	 houseArea;
	private int	 houseFloor;
	private PartEnum housePart;
	private OrientationEnum houseOrientation;
	private int	 hentWay;
	private String linkMan;
	private String releaseTime;
	private float	housePrice;
	private String fITMENTType;
	private String houseXQ;
	private String xQintroduce;
	private String houseDetails;
	private int locationID;
	private Location location;
	private Type housetype;
	private String houseImage;
	private String type;
	
	
	public int getLocationID() {
		return locationID;
	}
	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getHouseID() {
		return houseID;
	}
	public void setHouseID(int houseID) {
		this.houseID = houseID;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public int getHouseTypeID() {
		return houseTypeID;
	}
	public void setHouseTypeID(int houseTypeID) {
		this.houseTypeID = houseTypeID;
	}
	public int getHouseArea() {
		return houseArea;
	}
	public void setHouseArea(int houseArea) {
		this.houseArea = houseArea;
	}
	public int getHouseFloor() {
		return houseFloor;
	}
	public void setHouseFloor(int houseFloor) {
		this.houseFloor = houseFloor;
	}
	public PartEnum getHousePart() {
		return housePart;
	}
	public void setHousePart(PartEnum housePart) {
		
		this.housePart = housePart;
	}
	public OrientationEnum getHouseOrientation() {
		return houseOrientation;
	}
	public void setHouseOrientation(OrientationEnum houseOrientation) {
		this.houseOrientation = houseOrientation;
	}
	public int getHentWay() {
		return hentWay;
	}
	public void setHentWay(int hentWay) {
		this.hentWay = hentWay;
	}
	public String getLinkMan() {
		return linkMan;
	}
	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}
	public String getReleaseTime() {
		return releaseTime;
	}
	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}
	public float getHousePrice() {
		return housePrice;
	}
	public void setHousePrice(float housePrice) {
		this.housePrice = housePrice;
	}
	public String getfITMENTType() {
		return fITMENTType;
	}
	public void setfITMENTType(String fITMENTType) {
		this.fITMENTType = fITMENTType;
	}
	public String getHouseXQ() {
		return houseXQ;
	}
	public void setHouseXQ(String houseXQ) {
		this.houseXQ = houseXQ;
	}
	public String getxQintroduce() {
		return xQintroduce;
	}
	public void setxQintroduce(String xQintroduce) {
		this.xQintroduce = xQintroduce;
	}
	public String getHouseDetails() {
		return houseDetails;
	}
	public void setHouseDetails(String houseDetails) {
		this.houseDetails = houseDetails;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Type getHousetype() {
		return housetype;
	}
	public void setHousetype(Type housetype) {
		this.housetype = housetype;
	}
	public String getHouseImage() {
		return houseImage;
	}
	
	public void setHouseImage(String houseImage) {
		this.houseImage = houseImage;
	}
	@Override
	public String toString() {
		return "House [houseID=" + houseID + ", houseName=" + houseName + ", houseTypeID=" + houseTypeID
				+ ", houseArea=" + houseArea + ", houseFloor=" + houseFloor + ", housePart=" + housePart
				+ ", houseOrientation=" + houseOrientation + ", hentWay=" + hentWay + ", linkMan=" + linkMan
				+ ", releaseTime=" + releaseTime + ", housePrice=" + housePrice + ", fITMENTType=" + fITMENTType
				+ ", houseXQ=" + houseXQ + ", xQintroduce=" + xQintroduce + ", houseDetails=" + houseDetails
				+ ", locationID=" + locationID + ", location=" + location + ", housetype=" + housetype + ", houseImage="
				+ houseImage + ", type=" + type + "]";
	}
	
	
	
	
	
	
	
}
