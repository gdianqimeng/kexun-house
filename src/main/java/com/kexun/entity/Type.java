package com.kexun.entity;

public class Type {
	private int typeID;
	private String type;
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Type [typeID=" + typeID + ", type=" + type + "]";
	}

	

}
