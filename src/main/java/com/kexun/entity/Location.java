package com.kexun.entity;

public class Location {
	private int locationID;
	private String locationName;
	public int getLocationID() {
		return locationID;
	}
	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	@Override
	public String toString() {
		return "Location [locationID=" + locationID + ", locationName=" + locationName + "]";
	}
	
	
	
	
	
	
}
