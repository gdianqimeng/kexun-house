$(function() {

	$("#file").change(function() {

		var objurl = getObjectURL(this.files[0]);
		function getObjectURL(file) {
			var url = "";
			if (window.createObjectURL != undefined) {
				url = window.createObjectURL(file);
			} else if (window.URL != undefined) {
				url = window.URL.createObjectURL(file);
			} else if (window.webkitURL != undefined) {
				url = window.webkitURL.createObjectURL(file);
			}
			return url;
		}
		$("#img1").attr("src", objurl);
	});

	// 得到上传按钮
	$(".member_mod_buttom").click(function() {

		var nick = $("#title").val();
		if (nick == "") {

			$.message({
				message : '昵称不能为空',
				type : 'error'

			});

			return;
		}

		if ($(":radio:checked").length == 0) {

			$.message({
				message : '请选择你的性别',
				type : 'error'

			});

			return;
		}

		// 表单提交
		$.ajax({
			url : "/MyHouseRental/userModify",
			type : "post",
			data : new FormData($('#form')[0]),
			processData : false,
			contentType : false,
			beforeSend : function() {
				$(".member_mod_buttom").val("信息提交中");
				$(".member_mod_buttom").attr('disabled', true);

			},
			success : function(result) {
				$(".member_mod_buttom").val("完成修改");
				$(".member_mod_buttom").attr('disabled', false);
				if (result.success == "ok") {
					$.message({
						message : '信息修改成功',
						type : 'success',
						showClose : true

					});

					setTimeout(function() {

						window.location.href = "index";

					}, 3000);

				} else {
					$.message({
						message : '信息修改失败: ' + result.success,
						type : 'error',
						showClose : true

					});

				}

			}

		});

	});

});