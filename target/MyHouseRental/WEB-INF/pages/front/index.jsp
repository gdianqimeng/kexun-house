<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Author" contect="http://www.webqin.net">
    <title>邻居大妈</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}images/favicon.ico"/>
    <link type="text/css" href="${pageContext.request.contextPath}/css/css.css" rel="stylesheet"/>
    <link type="text/css" href="${pageContext.request.contextPath}/css/message.css" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/message.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/js.js"></script>
    <script type="text/javascript">
       var path ='${pageContext.request.contextPath}';
        //导航定位
        $(".nav li:eq(0)").addClass("navCur");

    </script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/index.js"></script>

</head>

<body>
<div class="header">
    <div class="width1190">
        <div class="fl">您好,${user["userName"] }欢迎来到邻居大妈！</div>
        <div class="fr">
            <a href="login">登录</a> |
            <a href="reg">注册</a> |
            <a href="javascript:;" onclick="AddFavorite(window.location,document.title)">加入收藏</a> |
            <a href="javascript:;" onclick="SetHome(this,window.location)">设为首页</a>
        </div>
        <div class="clears"></div>
    </div><!--width1190/-->
</div><!--header/-->
<div class="logo-phone">
    <div class="width1190">
        <h1 class="logo"><a href="index.html"><img src="images/logo.png" width="163" height="59"/></a></h1>
        <div class="phones"><strong>021-63179891</strong></div>
        <div class="clears"></div>
    </div><!--width1190/-->
</div><!--logo-phone/-->
<div class="list-nav">
    <div class="width1190">
        <div class="list">
            <h3>房源分类</h3>
            <div class="list-list">
                <dl>
                    <dt><a href="javascript:;">房源区域</a></dt>
                    <dd id="location">
                        <!-- location -->
                    </dd>
                </dl>
                <dl>
                    <dt><a href="zu">租房</a></dt>
                    <dd>
                        <a href="zu">租金</a>
                        <a href="zu">出租方式</a>
                        <a href="zu">面积</a>
                        <a href="zu">房型</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="xin">新房</a></dt>
                    <dd>
                        <a href="xin">价格</a>
                        <a href="xin">面积</a>
                        <a href="xin">房型</a>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="er">二手房</a></dt>
                    <dd>
                        <a href="er">价格</a>
                        <a href="er">面积</a>
                        <a href="er">房型</a>
                    </dd>
                </dl>
            </div>
        </div><!--list/-->
        <ul class="nav">
            <li><a href="index">首页</a></li>
            <li><a href="zu">租房</a></li>
            <li><a href="xin">新房</a></li>
            <li><a href="er">二手房</a></li>
            <li class="zhiding"><a href="javascript:;">指定购房</a></li>
            <li><a href="jingji">申请自由经纪人</a></li>
            <li><a href="about">关于我们</a></li>
            <div class="clears"></div>
        </ul><!--nav/-->
        <div class="clears"></div>
    </div><!--width1190/-->
</div><!--list-nav/-->
<div class="banner" style="background:url(images/ban.jpg) center center no-repeat;"></div>
<div class="content">
    <div class="width1190">
        <h2 class="title">租房 <a href="zu">更多&gt;&gt;</a></h2>
        <div class="index-fang-list">
            <div id="zhouseList">

            </div>
            <div class="clears"></div>
        </div><!--index-fang-list/-->

        <h2 class="title">新房 <a href="xin">更多&gt;&gt;</a></h2>
        <div class="index-fang-list">
            <div id="xhouseList">


            </div>

            <div class="clears"></div>
        </div><!--index-fang-list/-->

        <h2 class="title">二手房 <a href="er">更多&gt;&gt;</a></h2>
        <div class="index-ershou">
            <div id="ehouseList">


            </div>

        </div>

        <div class="footer">
            <div class="width1190">
                <div class="fl"><a href="index"><strong>邻居大妈</strong></a><a href="about">关于我们</a><a
                        href="contact">联系我们</a><a href="user">个人中心</a></div>
                <div class="fr">
                    <dl>
                        <dt><img src="images/erweima.png" width="76" height="76"/></dt>
                        <dd>微信扫一扫<br/>房价点评，精彩发布</dd>
                    </dl>
                    <dl>
                        <dt><img src="images/erweima.png" width="76" height="76"/></dt>
                        <dd>微信扫一扫<br/>房价点评，精彩发布</dd>
                    </dl>
                    <div class="clears"></div>
                </div>
                <div class="clears"></div>
            </div><!--width1190/-->
        </div><!--footer/-->
        <div class="copy">Copyright@ 2015 邻居大妈 版权所有 沪ICP备1234567号-0&nbsp;&nbsp;&nbsp;&nbsp;技术支持：<a target="_blank"
                                                                                                   href="http://www.webqin.net/">秦王网络</a>
        </div>
        <div class="bg100"></div>
        <div class="zhidinggoufang">
            <h2>指定购房 <span class="close">X</span></h2>
            <form action="specifybuy" method="post">
                <div class="zhiding-list">
                    <label>选择区域：</label>
                    <select name="locationID" id="locationID">
                        <option value="1">智慧园</option>
                        <option value="2">立民村</option>
                        <option value="3">塘口村</option>
                        <option value="4">勤劳村</option>
                        <option value="5">芦胜村</option>
                        <option value="6">知新村</option>
                    </select>
                </div>
                <div class="zhiding-list">
                    <label>方式：</label>
                    <select name="buyType" id="buyType">
                        <option value="租房">租房</option>
                        <option value="新房">新房</option>
                        <option value="二手房">二手房</option>
                    </select>
                </div>
                <div class="zhiding-list">
                    <label>联系方式：</label>
                    <input type="text" name="phone" id="phone"/>
                </div>
                <div class="zhidingsub"><input type="button" value="提交" id="sub"/></div>
            </form>
            <div class="zhidingtext">
                <h3>指定购房注意事宜：</h3>
                <p>1、请详细输入您所需要购买的房源信息(精确到小区)</p>
                <p>2、制定购房申请提交后，客服中心会在24小时之内与您取得联系</p>
                <p>3、如有任何疑问，请随时拨打我们的电话：400-000-0000</p>
            </div><!--zhidingtext/-->
        </div><!--zhidinggoufang/-->
</body>
</html>
