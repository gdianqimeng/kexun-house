<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
     <meta charset="utf-8" />
     <title>后台管理系统</title>
     <meta name="author" content="DeathGhost" />
     <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css">
	     <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/message.css">
	
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/js/message.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.mCustomScrollbar.concat.min.js"></script>
</head>

<body>
     <!--header-->
     <header>
          <h1><img src="${pageContext.request.contextPath}/images/admin_logo.png" /></h1>
          <ul class="rt_nav">
               <li><a href="#" target="_blank" class="website_icon">站点首页</a></li>
               <li><a href="#" class="set_icon">账号设置</a></li>
             <li><a href="${pageContext.request.contextPath}/manage/loginOut" class="quit_icon">安全退出</a></li>
          </ul>
     </header>
     <!--aside nav-->
     <!--aside nav-->
     <aside class="lt_aside_nav content mCustomScrollbar">
       <h2>
			<a href="${pageContext.request.contextPath}/manage">起始页</a>
		</h2>
          <ul>
               <li>
                     <dl>
					<dt>房屋管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/zu">出租房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/er" class="active">二手房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/xin">全新房</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/houseAdd">发布信息</a>
					</dd>

				</dl>

				<dl>
					<dt>区域管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/location_list">区域列表</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/locationAdd">添加区域</a>
					</dd>

				</dl>

				<dl>
					<dt>预约管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/yuyue">预约列表</a>
					</dd>


				</dl>

				<dl>
					<dt>用户管理</dt>
					<!--当前链接则添加class:active-->
					<dd>
						<a href="${pageContext.request.contextPath}/manage/user_list">用户列表</a>
					</dd>
					<dd>
						<a href="${pageContext.request.contextPath}/manage/user_detail">添加管理员</a>
					</dd>


				</dl>
               </li>

            
          </ul>
     </aside>

     <section class="rt_wrap content mCustomScrollbar">
          <div class="rt_content">
               <div class="page_title">
                    <h2 class="fl">房屋列表</h2>
                    <a href="houseAdd.html" class="fr top_rt_btn add_icon">发布信息</a>
               </div>
               <section class="mtb">
                    <select class="select">
                         <option>下拉菜单</option>
                         <option>菜单1</option>
                    </select>
                    <input type="text" class="textbox textbox_225" placeholder="输入产品关键词或产品货号..."  id="hName"/>
                    <input type="button" value="查询" class="group_btn" id="query"/>
               </section>
              <table class="table">
				<tr>
					<th>缩略图</th>
					<th>房屋标题</th>
					<th>房屋面积(平方)</th>
					<th>租金</th>
					<th>所在区域</th>
					<th>装修类型</th>
					<th>房屋楼层</th>
					<th>发布时间</th>
					<th>操作</th>
				</tr>
		
				<c:forEach items="${erList}" var="house">
				<tr>
					<td class="center"><img src="/images/${house.houseImage }" width="50"
						height="50" /></td>
					<td>${house.houseName }</td>
					<td class="center">${house.houseArea }</td>
					<td class="center"><strong class="rmb_icon">${house.housePrice}</strong></td>
					<td class="center">${house.location.locationName }</td>
					<td class="center">${house.housetype.type }</td>
					<td class="center">${house.houseFloor}</td>
					<td class="center">${house.releaseTime}</td>

					<td class="center"><a href="houseModify?id=${house.houseID }" title="编辑"
						class="link_icon">&#101;</a> <a href="javascript:del(${house.houseID })" title="删除"
						class="link_icon">&#100;</a></td>
				</tr>
				
				</c:forEach>
				

			</table>
              <aside class="paging" id="page">
				<!-- <a>第一页</a> <a>1</a> <a>2</a> <a>3</a> <a>…</a> <a>1004</a> <a>最后一页</a> -->
			</aside>
          </div>
     </section>
</body>
<script type="text/javascript">
	
		function del(id) {
			if (confirm("确定要删除吗?")) {
				$.get("/MyHouseRental/manage/delhouse",{"id":id},function(result){
					
					if(result.message=="success"){
						$.message({
							type:'success',
							message:'删除成功'
							
						});
						
						setTimeout(function(){
							 location.reload();
							
							
						},2000)
						
					}else{
						$.message({
							type:'error',
							message:'系统错误'
							
						});
						
						
					}
					
					
				});
			}

		}

		$(function() {
			var pageCount = ${pageCount};
			var page=${page};
			
			$("#page").append("<a href='er?page=1'>第一页</a>");
				if(page-1>0){
					$("#page").append("<a href='er?page="+(page-1)+"'>上一页</a>");	
				}
				
				
			if((pageCount-page+1)>=5){
			
				for (var i = page; i < page+5; i++) {
					if(page==i){
						$("#page").append("<a style='background-color: lightcoral;' href='er?page="+i+"'>"+i+"</a>");	
						
					}else{
						$("#page").append("<a href='er?page="+i+"'>"+i+"</a>");	
					}
					
				}
			}else if(pageCount-4>0){
				
				for (var i = pageCount-4; i <= pageCount; i++) {
					if(page==i){
						$("#page").append("<a style='background-color: lightcoral;' href='er?page="+i+"'>"+i+"</a>");	
						
					}else{
						$("#page").append("<a href='er?page="+i+"'>"+i+"</a>");	
					}
					
				}
			}else{
				for (var i = 1; i <= pageCount; i++) {
					if(page==i){
						$("#page").append("<a style='background-color: lightcoral;' href='er?page="+i+"'>"+i+"</a>");	
						
					}else{
						$("#page").append("<a href='er?page="+i+"'>"+i+"</a>");	
					}
					
				}
				
				
			}
				
			
			
			
				if(page+1<=pageCount){
					$("#page").append("<a href='er?page="+(page+1)+"'>下一页</a>");	
				}
				$("#page").append("<a href='er?page="+pageCount+"'>最后一页</a>");
				

				$("#query").click(function(){
					var hName=$("#hName").val();
					
					window.location.href="?hName="+hName;
					
					
					
				});
		});
	
</script>
</html>